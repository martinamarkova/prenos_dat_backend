package sk.fri.uniza.auth;

import io.dropwizard.auth.Authorizer;
import io.dropwizard.hibernate.UnitOfWork;
import sk.fri.uniza.api.MeteoStation;

/**
 * slúži ako kontrola autorizácie, či daná metoestanica má povolenie zapisovať do DB
 */
public class MeteoAuthorizer implements Authorizer<MeteoStation> {

    /**
     * defaultný konštruktor
     */
    public MeteoAuthorizer(){
    }

    /**
     * overí či daná meteostanica má prístup
     * @param meteoStation
     * @param apiKey
     * @return true ak je prístup povolený
     */
    @Override
    @UnitOfWork
    public boolean authorize(MeteoStation meteoStation, String apiKey) {
        //if(true){return true;}
        if (meteoStation.getApiKey() == null) return false;
        return meteoStation.getApiKey().toString().contains(apiKey);
    }
}
