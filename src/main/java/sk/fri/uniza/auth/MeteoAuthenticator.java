package sk.fri.uniza.auth;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.db.MeteoStationDao;

import java.util.Optional;

/**
 * trieda slúži ako autentifikátor, kontroluje či daná meteostanica má vytvorený účet
 */
public class MeteoAuthenticator implements Authenticator<BasicCredentials, MeteoStation> {
    //static MeteoStations meteoStations = new MeteoStations();

    private static final Logger LOG = LoggerFactory.getLogger(MeteoAuthenticator.class);
    private MeteoStationDao meteoStationDao;

    /**
     * konštruktor ktorý vytvori autentifikátor a vloží mu referenciu na objekt pracujúci s databázov
     * @param meteoStationDao
     */
    public MeteoAuthenticator(MeteoStationDao meteoStationDao) {
        this.meteoStationDao = meteoStationDao;
    }

    /**
     * metóda pôvodne slúžila na zapísanie staticky vytvorených objektov do databázy
     */
    @UnitOfWork
    public void generateMeteoStation() {
        MeteoStationDao.getMeteoStationDB().forEach(meteoStationDao::save);
    }

    /**
     * metoda sa volá automaticky vždy keď je nutná autentifikácia
     * ak je úspešná tak vráti usra najdeneho v DB ak nie vrati empty
     * @param credentials
     * @return
     * @throws AuthenticationException
     */
    @Override
    @UnitOfWork
    public Optional<MeteoStation> authenticate(BasicCredentials credentials) throws AuthenticationException {
        //Optional<MeteoStation> meteoStation = meteoStations.findById(credentials.getUsername());
        meteoStationDao.loadMeteoStastion();
        Optional<MeteoStation> meteoStation = meteoStationDao.findByName(credentials.getUsername());
        if (meteoStation.isPresent()) {
            if (meteoStation.get().getPassword().equals(credentials.getPassword())) {
                return meteoStation;
            }
        }

        return Optional.empty();
    }
}
