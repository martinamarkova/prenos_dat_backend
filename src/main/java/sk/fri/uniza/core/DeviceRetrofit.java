package sk.fri.uniza.core;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * zoznam metod ktoré je možné volať na zariadenei
 */
public interface DeviceRetrofit {
    @POST("getShortWeather")
    Call<MyWeatherData> getWeatherData();

}
