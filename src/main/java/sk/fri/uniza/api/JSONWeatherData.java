package sk.fri.uniza.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import sk.fri.uniza.core.MyWeatherData;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * trieda, ktorá reprezentuje objekt dát o počasí, ktoré sa posielajú zo zariadenia na backend aleebo z backendu na web,
 * tieto dáta sa ukladajú do databázy
 */
@Entity
@Table(name = "WeatherData")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NamedQueries(
        {
                @NamedQuery(
                        name = "sk.fri.uniza.api.getAllJSONWeatherData",
                        query = "SELECT w FROM JSONWeatherData w"
                )
        })
public class JSONWeatherData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Column(unique = true)
    @JsonProperty("idu")
    @Column(unique = true)
    private Long idu;

    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String country;
    @JsonProperty
    private String description;
    @JsonProperty
    private Double temp;
    @JsonProperty
    private Double temp_min;
    @JsonProperty
    private Double temp_max;
    @JsonProperty
    private Integer cloudiness;
    @JsonProperty
    private Integer pressure;
    @JsonProperty
    private Integer humidity;
    @JsonProperty
    private Double wind_speed;
    @JsonProperty
    private String added;

    /**
     * default konštruktor
     */
    public JSONWeatherData() {
    }

    /**
     * konštruktor, ktorý zabezpečí inicializáciu dát pre počasie z parametrov
     * @param id, id pre meteostanicu, kt. vytvorila tieto dáta
     * @param name
     * @param country
     * @param description
     * @param temp
     * @param temp_min
     * @param temp_max
     * @param cloudiness
     * @param pressure
     * @param humidity
     * @param wind_speed
     */
    public JSONWeatherData(Long id, String name, String country, String description, Double temp, Double temp_min, Double temp_max, Integer cloudiness, Integer pressure, Integer humidity, Double wind_speed) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.description = description;
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.cloudiness = cloudiness;
        this.pressure = pressure;
        this.humidity = humidity;
        this.wind_speed = wind_speed;
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        this.added = formatter.format(date);
    }

    /**
     * konštruktor, kt. vytvorí objekt JSONWeatherData s objektu MyWeatherData, kt. obsahuje v sebe skrátenú formu informácií
     * o počasí
     * @param data
     */
    public JSONWeatherData(MyWeatherData data) {
        this.id = data.getId();
        this.name = data.getName();
        this.country = data.getCountry();
        this.description = data.getDescription();
        this.temp = data.getTemp();
        this.temp_min = data.getTempMin();
        this.temp_max = data.getTempMax();
        this.cloudiness = data.getCloudiness();
        this.pressure = data.getPressure();
        this.humidity = data.getHumidity();
        this.wind_speed = data.getWindSpeed();
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        this.added = formatter.format(date);
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("temp")
    public Double getTemp() {
        return temp;
    }

    @JsonProperty("temp")
    public void setTemp(Double temp) {
        this.temp = temp;
    }

    @JsonProperty("temp_min")
    public Double getTemp_min() {
        return temp_min;
    }

    @JsonProperty("temp_min")
    public void setTemp_min(Double temp_min) {
        this.temp_min = temp_min;
    }

    @JsonProperty("temp_max")
    public Double getTemp_max() {
        return temp_max;
    }

    @JsonProperty("temp_max")
    public void setTemp_max(Double temp_max) {
        this.temp_max = temp_max;
    }

    @JsonProperty("cloudiness")
    public Integer getCloudiness() {
        return cloudiness;
    }

    @JsonProperty("cloudiness")
    public void setCloudiness(Integer cloudiness) {
        cloudiness = cloudiness;
    }

    @JsonProperty("pressure")
    public Integer getPressure() {
        return pressure;
    }

    @JsonProperty("pressure")
    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    @JsonProperty("humidity")
    public Integer getHumidity() {
        return humidity;
    }

    @JsonProperty("humidity")
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    @JsonProperty("wind_speed")
    public Double getWind_speed() {
        return wind_speed;
    }

    @JsonProperty("wind_speed")
    public void setWind_speed(Double wind_speed) {
        this.wind_speed = wind_speed;
    }

    @JsonProperty("added")
    public String getAdded() {
        return added;
    }

    @JsonProperty("added")
    public void setAdded(String registered) {
        this.added = registered;
    }
}
