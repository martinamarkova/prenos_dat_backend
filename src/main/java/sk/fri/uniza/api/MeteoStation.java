package sk.fri.uniza.api;

import com.fasterxml.jackson.annotation.*;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * objekt MeteoStanice, kt. obsahuje všetky dôležité infomácie o stanici(zariadení)
 */
@Entity
@Table(name = "MeteoStation")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NamedQueries(
        {
                @NamedQuery(
                        name = "sk.fri.uniza.api.getAllMeteoStation",
                        query = "SELECT ms FROM MeteoStation ms"
                )
        })
public class MeteoStation implements Principal {
    @Id
    @JsonProperty
    @Column(unique = true)
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String password;
    @JsonProperty
    private String location;
    @Column
    @ElementCollection(targetClass = String.class)
    @JsonProperty
    private Set<String> apiKey;
    @JsonProperty
    private String registered;
    @JsonProperty
    private Integer port;

    /**
     * default konštruktor
     */
    public MeteoStation(){}

    /**
     * parametrický konštruktor, ktorý inicializuje objekt MeteoStanice pomocou parametrov
     * @param name
     * @param password
     * @param location
     * @param id
     * @param apiKey
     * @param port
     */
    public MeteoStation(String name, String password, String location, Long id, Set<String> apiKey, Integer port){
        this.name = name;
        this.password = password;
        this.location = location;
        this.id = id;
        this.apiKey = apiKey;
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        this.registered = formatter.format(date);
        this.port = port;
    }

    /**
     * konštruktor, kt. vytvorí meteostanicu z inej meteostanice
     * @param meteoStation
     */
    public MeteoStation(MeteoStation meteoStation){
        this.name = meteoStation.getName();
        this.password = meteoStation.getPassword();
        this.location = meteoStation.getLocation();
        this.id = meteoStation.getId();
        //this.apiKey = meteoStation.getApiKey();
        this.apiKey = Set.of(meteoStation.getApiKey().toString());
        this.registered = meteoStation.getRegistered();
        this.port = meteoStation.getPort();
    }

    /**
     *
     * @return meno stanice
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * setter pre nastavenie mena stanice
     * @param name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return password
     */
    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    /**
     * setter pre nastavenie passwordu
     * @param password
     */
    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return location
     */
    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    /**
     * setter pre nastavenie location
     * @param location
     */
    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     *
     * @return id stanice
     */
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     * setter pre nastavenie id stanice
     * @param id
     */
    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return apiKey
     */
    @Column
    @ElementCollection(targetClass = String.class,fetch = FetchType.EAGER)
    @JsonProperty("apiKey")
    public Set<String> getApiKey() {
        return apiKey;
    }

    /**
     * setter pre nastavenie api kľúča
     * @param apiKey
     */
    @JsonProperty("apiKey")
    public void setApiKey(Set<String> apiKey) {
        this.apiKey = apiKey;
    }

    /**
     *
     * @return registrovanie konkrétnej stanice
     */
    @JsonProperty("registered")
    public String getRegistered() {
        return registered;
    }

    /**
     * setter pre nastavenie registrácie stanice
     * @param registered
     */
    @JsonProperty("registered")
    public void setRegistered(String registered) {
        this.registered = registered;
    }

    /**
     *
     * @return port
     */
    @JsonProperty("port")
    public Integer getPort() {
        return port;
    }

    /**
     * setter pre nastavenie portu
     * @param port
     */
    @JsonProperty("port")
    public void setPort(Integer port) {
        this.port = port;
    }
}
