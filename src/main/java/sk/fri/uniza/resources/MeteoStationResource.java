package sk.fri.uniza.resources;

import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.fri.uniza.api.Location;
import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Paged;
import sk.fri.uniza.api.Person;
import sk.fri.uniza.auth.Role;
import sk.fri.uniza.core.User;
import sk.fri.uniza.db.MeteoStationDao;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * slúži ak rozhranie pre prístup k funkciam pomocou REST nazáklade http žiadostí
 */
@Path("/meteoStation")
@Api(value = "Meteostanica")
public class MeteoStationResource {
    private final Logger myLogger = LoggerFactory.getLogger(this.getClass());
    private MeteoStationDao meteoStationDao;

    public MeteoStationResource (MeteoStationDao meteoStationDao) {
        this.meteoStationDao = meteoStationDao;
    }

    /**
     * manualne pridávanie meteostanice (bez žiadnej kontroly) vyúživalo sa to na testovacie účely pomocou postmena
     * @param name
     * @param password
     * @param location
     * @param id
     * @param apiKey
     * @param port
     * @return
     */
    @GET
    @Path("/addSensor")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Pridanie novej meteostanice." )
    public MeteoStation add(@QueryParam("name") String name,
                            @QueryParam("password") String password,
                            @QueryParam("location") String location,
                            @QueryParam("id")Long id,
                            @QueryParam("apikey") Set<String> apiKey,
                            @QueryParam("port") Integer port) {
        MeteoStation meteoStation = new MeteoStation(name, password, location, id, apiKey, port);
        meteoStationDao.save(meteoStation);
        MeteoStationDao.getMeteoStationDB().add(meteoStation);
        return meteoStation;
    }

    /**
     * metóda na základe id stanice sa pokúsi vymazať meteostanicu
     * vymazanie bude úspešné iba ak sa meteostanica nachádza v databáze
     * a súčasne užívateľ, ktorý sa pokúša o vymazanie je adminom
     * @param user
     * @param id
     * @return
     */
    @DELETE
    @UnitOfWork
    //@RolesAllowed(Role.ADMIN)
    @ApiOperation(value = "vymazanie meteostanice", response = MeteoStation.class, authorizations = {@Authorization(value = "oauth2", scopes = {@AuthorizationScope(scope = Role.ADMIN, description = "Access to all resources")})})
    public Response deleteDevice(@ApiParam(hidden = true) @Auth User user, @QueryParam("id") Long id) {
        if (!user.getRoles().contains(Role.ADMIN)) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
        //if (user.getId() != id) {
            Optional<MeteoStation> meteoStation = meteoStationDao.findById(id);
            if (meteoStation.isPresent()) {
                return meteoStation.map(station -> {
                    meteoStationDao.delete(station);
                    return Response.ok().build();
                }).get();
            }
        //}
        return Response.status(Response.Status.BAD_REQUEST).build();
    }


    /**
     * vráti zoznam meteostaníc v stránkach
     * @param limit počet meteostaníc na stránke
     * @param page
     * @return
     */
    @GET
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    // Swagger
    @ApiOperation(value = "Získa zoznam zariadení(meteostaníc)", response = MeteoStation.class, responseContainer = "List")
    public Response getListOfStations(@QueryParam("limit") Integer limit, @QueryParam("page") Integer page) {
        if (page == null) page = 1;
        if (limit != null) {
            Paged<List<MeteoStation>> listPaged = meteoStationDao.getAll(limit, page);
            return Response.ok()
                    .entity(listPaged)
                    .build();

        } else {
            List<MeteoStation> meteoList = meteoStationDao.getAll();
            return Response.ok()
                    .entity(meteoList)
                    .build();
        }

    }

    /**
     * tento resource sa volá ak sa pokúša používateľ na webe pridať meteostanicu
     * @param user user, ktorý sa musí autentifikovať
     * @param name prihlasovacie meno
     * @param password prihlasovacie heslo
     * @param apiKey
     * @return
     */
    @POST
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Uloženie meteostanice do databázy", response = MeteoStation.class, authorizations = {@Authorization(value = "oauth2", scopes = {@AuthorizationScope(scope = Role.ADMIN, description = "prístup k všetkým resourcom")})})
    public MeteoStation setMeteoStationInfo(@ApiParam(hidden = true) @Auth User user, @QueryParam("name") String name, @QueryParam("password") String password, @QueryParam("apiKey") Set<String> apiKey) {

        if (!user.getRoles().contains(Role.ADMIN)) {
                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
        if (Location.getLocationByCity(name) == Location.NONE) {
            throw new WebApplicationException("nesprávne vstupné dáta (skús iné meno name)", 422);
        }
            // If no id is presented, person is saved as new instance
        MeteoStation meteoStation = new MeteoStation(name,
                    password,
                Location.getLocationByCity(name).getLocationToString(),
                Location.getLocationByCity(name).getId(),
                apiKey,
                Location.getLocationByCity(name).getAppConectPort());

            try {
                meteoStationDao.save(meteoStation);
            } catch (HibernateException e) {
                throw new WebApplicationException(e.getMessage(), 422); //422 Unprocessable Entity - validation like errors
            }
        return meteoStation;
    }
}
