package sk.fri.uniza.resources;

import com.codahale.metrics.annotation.Timed;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.fri.uniza.api.JSONWeatherData;
import sk.fri.uniza.api.Location;
import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Paged;
import sk.fri.uniza.auth.APIKEYS;
import sk.fri.uniza.auth.Role;
import sk.fri.uniza.core.MyWeatherData;
import sk.fri.uniza.db.JSONWeatherDataDao;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static sk.fri.uniza.db.JSONWeatherDataDao.getJSONWeatherDataDao;

@Path("/weatherData")
@Api(value = "Počasie")
public class JSONWeatherDataResource {

    private final Logger myLogger = LoggerFactory.getLogger(this.getClass());
    private JSONWeatherDataDao weatherDataDao;

    /**
     * vetvorí nový JSON weatherData resource a vloží referenciu na objekt pracujúci s DB
     * @param weatherDataDao
     */
    public JSONWeatherDataResource (JSONWeatherDataDao weatherDataDao) {
        this.weatherDataDao = weatherDataDao;
    }

    /**
     * pri volaní resourcu sa uložia dáta na server
     * ale až po úspešnéj autentifikácii a autorizácii meteostanice
     * @param meteoStation
     * @param id
     * @param name
     * @param country
     * @param description
     * @param temp
     * @param tempMin
     * @param tempMax
     * @param cloudiness
     * @param pressure
     * @param humidity
     * @param windSpeed
     * @return
     */
    @GET
    @Path("/saveWeather")
    @UnitOfWork()
            @RolesAllowed({APIKEYS.API_KEY_ZILINA,
            APIKEYS.API_KEY_BANSKABYSTRICA,
            APIKEYS.API_KEY_BRATISLAVA,
            APIKEYS.API_KEY_KOSICE,
            APIKEYS.API_KEY_NITRA,
            APIKEYS.API_KEY_PRESOV,
            APIKEYS.API_KEY_TRENCIN,
            APIKEYS.API_KEY_TRNAVA})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Uloženie dát do databázy.", authorizations = {@Authorization(value = "Basic", scopes = {@AuthorizationScope(scope = "APIKEY", description = "prístup zariadení s apikľúčom")})})
    public MeteoStation saveWeather(@Auth MeteoStation meteoStation,
                                    @QueryParam("id") Long id,
                                    @QueryParam("name") String name,
                                    @QueryParam("country") String country,
                                    @QueryParam("description") String description,
                                    @QueryParam("temp") Double temp,
                                    @QueryParam("temp_min") Double tempMin,
                                    @QueryParam("temp_max") Double tempMax,
                                    @QueryParam("Cloudiness") Integer cloudiness,
                                    @QueryParam("pressure") Integer pressure,
                                    @QueryParam("humidity") Integer humidity,
                                    @QueryParam("wind_speed") Double windSpeed){
        MyWeatherData myWeatherData = new MyWeatherData(id, name, country, description, temp, tempMin, tempMax, cloudiness, pressure, humidity, windSpeed);
        JSONWeatherData myJsonData = new JSONWeatherData(myWeatherData);
        weatherDataDao.save(myJsonData);
        //return meteoStation;
        return new MeteoStation();
        //  @QueryParam("my_weatherData") Optional<MyWeatherData> myWeatherData
    }



    /**
     * vráti stránkovaný zoznam počasia pre danú stanicu podla jej ID
     * @param limit
     * @param page stránka ktorú chceme
     * @param id
     * @return
     */
    @GET
    @Path("/byId")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //@RolesAllowed({Role.ADMIN, Role.USER_READ_ONLY})

    // Swagger
    @ApiOperation(value = "Získanie listu počasia podľa ID stanice", response = JSONWeatherData.class, responseContainer = "List")
    public Response getListOfWeatherById(@QueryParam("limit") Integer limit, @QueryParam("page") Integer page, @QueryParam("id") Long id) {
        if (page == null) page = 1;
        if (limit != null) {
            Paged<List<JSONWeatherData>> listPaged = weatherDataDao.getbyMeteoStationID(limit, page, id);
            return Response.ok()
                    .entity(listPaged)
                    .build();

        } else {
            List<JSONWeatherData> weatherList = weatherDataDao.getAll();
            return Response.ok()
                    .entity(weatherList)
                    .build();
        }

    }

    /**
     * vyžiada si zoznam celého počasia
     * @return
     */
    @GET
    @Path("/allWeather")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    // Swagger
    @ApiOperation(value = "Vráti list obshujúci všetky záznami o počasí", response = JSONWeatherData.class, responseContainer = "List")
    public Response getListOfWeatherById() {
            List<JSONWeatherData> list = weatherDataDao.getAll();
            return Response.ok()
                    .entity(list)
                    .build();

    }
}
