package sk.fri.uniza.resources;


import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import io.dropwizard.auth.Auth;
import io.dropwizard.auth.basic.BasicCredentials;
import io.dropwizard.jersey.sessions.Session;
import sk.fri.uniza.WindFarmDemoApplication;
import sk.fri.uniza.api.JSONWeatherData;
import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Saying;
import sk.fri.uniza.auth.Role;
import sk.fri.uniza.core.MyWeatherData;
import sk.fri.uniza.core.User;
import sk.fri.uniza.db.MeteoStationDao;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static sk.fri.uniza.db.JSONWeatherDataDao.getJSONWeatherDataDao;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public HelloWorldResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    @GET
    @Path("/hello-world")
    @Timed
    @RolesAllowed(Role.ADMIN)
    public Saying sayHello(@QueryParam("name") Optional<String> name) {
        final String value = String.format(template, name.orElse(defaultName));
        return new Saying(counter.incrementAndGet(), value);
    }

    @GET
    @Path("/hello-user")
    @Timed
    @RolesAllowed({Role.ADMIN, Role.USER_READ_ONLY})
    public Saying sayHello(@Auth User user) {
        final String value = String.format(template, user.getName());
        return new Saying(counter.incrementAndGet(), value);
    }

    @GET
    @Path("/hello-meteo")
    @Timed
    @RolesAllowed({Role.ADMIN, Role.USER_READ_ONLY})
    public Saying sayHello(@Auth MeteoStation user) {
        final String value = String.format(template, user.getName());
        return new Saying(counter.incrementAndGet(), value);
    }

    @POST
    @Path("/loginMeteo")
    @Timed
    public MeteoStation loginMeteo(@Auth MeteoStation meteoStation) {
        return meteoStation;
    }



    @GET
    @Path("/sessiontest")
    @Timed
    @ExceptionMetered
    public Response getMain(@Context UriInfo info, @Session HttpSession session) {

        return Response.ok().build();
    }

    @GET
    @Path("/testGetWeather")
    @Timed
    //@ExceptionMetered
    public JSONWeatherData getWeather() {

        try {               //App.getRequest.... vráti retrofitClienta
            retrofit2.Response<MyWeatherData> weatherResp = WindFarmDemoApplication.getDeviceRetrofit().getWeatherData().execute();
            MyWeatherData dataWeather;
            //MyWeatherData myWeatherData;
            if (weatherResp.isSuccessful()) {
                dataWeather = weatherResp.body();
                return new JSONWeatherData(dataWeather);
            } else {
                dataWeather = new MyWeatherData();
                dataWeather.setName(weatherResp.toString());
                return new JSONWeatherData(dataWeather);
            }

        } catch (IOException e) {
            JSONWeatherData jSONWeatherData = new JSONWeatherData();
            jSONWeatherData.setName("Exception");
            return null;
        }
    }

}