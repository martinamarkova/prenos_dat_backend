package sk.fri.uniza;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.*;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWorkAwareProxyFactory;
import io.dropwizard.jersey.errors.ErrorEntityWriter;
import io.dropwizard.jersey.errors.ErrorMessage;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.View;
import io.dropwizard.views.ViewBundle;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.hibernate.SessionFactory;
import sk.fri.uniza.api.*;
import sk.fri.uniza.auth.*;
import sk.fri.uniza.config.WindFarmDemoConfiguration;
import sk.fri.uniza.core.DeviceRetrofit;
import sk.fri.uniza.core.User;
import sk.fri.uniza.db.JSONWeatherDataDao;
import sk.fri.uniza.db.MeteoStationDao;
import sk.fri.uniza.db.PersonDao;
import sk.fri.uniza.db.UsersDao;
import sk.fri.uniza.health.TemplateHealthCheck;
import sk.fri.uniza.resources.*;
import sk.fri.uniza.views.ErrorView;

import javax.ws.rs.core.MediaType;
import java.security.Key;
import java.security.KeyPair;
import java.util.Map;
//import java.util.timer.scheduleAtFixedRate;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class WindFarmDemoApplication extends Application<WindFarmDemoConfiguration> {
    public static DeviceRetrofit deviceRetrofit;

    public static DeviceRetrofit getDeviceRetrofit(){
        return deviceRetrofit;
    }

    public static void main(final String[] args) throws Exception {
        new WindFarmDemoApplication().run(args);
    }

    @Override
    public String getName() {
        return "WindFarmDemo";
    }


    /**
     * Initialization of Hibernate ORM bundle.
     * Note: Add class that need to be mapped by Hibernate
     */
    private final HibernateBundle<WindFarmDemoConfiguration> hibernate = new HibernateBundle<WindFarmDemoConfiguration>(MeteoStation.class, User.class, Person.class, Phone.class, JSONWeatherData.class) {

        @Override
        public PooledDataSourceFactory getDataSourceFactory(WindFarmDemoConfiguration windFarmDemoConfiguration) {
            return windFarmDemoConfiguration.getDataSourceFactory();
        }

        @Override
        public SessionFactory getSessionFactory() {
            return super.getSessionFactory();
        }
    };


    @Override
    public void initialize(final Bootstrap<WindFarmDemoConfiguration> bootstrap) {


        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));

        // Add View html bundle
        bootstrap.addBundle(new ViewBundle<WindFarmDemoConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(WindFarmDemoConfiguration configuration) {
                return configuration.getViewRendererConfiguration();
            }
        });

        // Add ORM Hibernate bundle
        bootstrap.addBundle(hibernate);

        // Swagger documentation available on http://localhost:<your_port>/swagger
        bootstrap.addBundle(new SwaggerBundle<WindFarmDemoConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(WindFarmDemoConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

    @Override
    public void run(final WindFarmDemoConfiguration configuration,
                    final Environment environment) {

        //Add HealthChecks
        final TemplateHealthCheck templateHealthCheck = new TemplateHealthCheck(configuration.getTemplate());

        environment.healthChecks().register("templateHealthCheck", templateHealthCheck);

        // Register Resources
        registerResources(configuration, environment);
        JSONWeatherDataDao.createJSONWeatherDataDao(hibernate.getSessionFactory());
        // Setup user auth
        registerAuth(configuration, environment);



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost:8011/meteo/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        deviceRetrofit = retrofit.create(DeviceRetrofit.class);


        environment.jersey().register(new ErrorEntityWriter<ErrorMessage, View>(MediaType.TEXT_HTML_TYPE, View.class) {
            @Override
            protected View getRepresentation(ErrorMessage errorMessage) {
                return new ErrorView(errorMessage);
            }
        });
    }

    private void registerAuth(WindFarmDemoConfiguration configuration, Environment environment) {

        // Load key that is used to sign the JWT token
        KeyPair secreteKey = configuration.getOAuth2Configuration().getSecreteKey(false);

        // UsersDAO
        final UsersDao usersDao = UsersDao.createUsersDao(hibernate.getSessionFactory());
        final MeteoStationDao meteoStationDao = MeteoStationDao.createMeteoDao(hibernate.getSessionFactory());

        // Initialize Basic authorization mechanism
        MeteoAuthenticator meteoAuthenticator = new UnitOfWorkAwareProxyFactory(hibernate).create(MeteoAuthenticator.class, new Class[]{MeteoStationDao.class}, new Object[]{meteoStationDao});
        AuthFilter basicCredentialAuthFilter = new BasicCredentialAuthFilter.Builder<MeteoStation>()
                .setAuthenticator(meteoAuthenticator)
                .setAuthorizer(new MeteoAuthorizer())
                .setRealm("SUPER SECRET STUFF")
                .buildAuthFilter();
        // Initialize OAuth 2 authorization mechanism
        OAuth2Authenticator oAuth2Authenticator = new UnitOfWorkAwareProxyFactory(hibernate).create(OAuth2Authenticator.class, new Class[]{UsersDao.class, Key.class}, new Object[]{usersDao, secreteKey.getPublic()});
        AuthFilter oauthCredentialAuthFilter = new OAuthCredentialAuthFilter.Builder<User>()
                .setAuthenticator(oAuth2Authenticator)
                .setAuthorizer(new OAuth2Authorizer())
                .setPrefix("Bearer")
                .buildAuthFilter();

                // Generate fake users
        //oAuth2Authenticator.generateUsers();
                // Crete One MeteoStation
        //meteoAuthenticator.generateMeteoStation();


        final PolymorphicAuthDynamicFeature feature = new PolymorphicAuthDynamicFeature<>(
                ImmutableMap.of(
                        MeteoStation.class, basicCredentialAuthFilter,
                        User.class, oauthCredentialAuthFilter));
        final AbstractBinder binder = new PolymorphicAuthValueFactoryProvider.Binder<>(
                ImmutableSet.of(MeteoStation.class, User.class));

        environment.jersey().register(RolesAllowedDynamicFeature.class);

        environment.jersey().register(feature);
        environment.jersey().register(binder);




    }

    private void registerResources(WindFarmDemoConfiguration configuration, Environment environment) {

        final HelloWorldResource helloWorldResource = new HelloWorldResource(configuration.getTemplate(), configuration.getDefaultName());

        // Create Dao access objects
        final UsersDao usersDao = UsersDao.createUsersDao(hibernate.getSessionFactory());
        final PersonDao personDao = new PersonDao(hibernate.getSessionFactory());
        final MeteoStationDao meteoStationDao = new MeteoStationDao(hibernate.getSessionFactory());
        final JSONWeatherDataDao weatherDataDao = new JSONWeatherDataDao(hibernate.getSessionFactory());

        KeyPair secreteKey = configuration.getOAuth2Configuration().getSecreteKey(false);
        final LoginResource loginResource = new LoginResource(secreteKey, usersDao, OAuth2Clients.getInstance());
        final UsersResource usersResource = new UsersResource(usersDao);
        final PersonResource personResource = new PersonResource(personDao);
        final MeteoStationResource meteoStationResource = new MeteoStationResource(meteoStationDao);
        final JSONWeatherDataResource weatherDataResource = new JSONWeatherDataResource(weatherDataDao);

        environment.jersey().register(helloWorldResource);
        environment.jersey().register(loginResource);
        environment.jersey().register(usersResource);
        environment.jersey().register(personResource);
        environment.jersey().register(meteoStationResource);
        environment.jersey().register(weatherDataResource);
        //meteoStationResource.add("poprad", "password" , "Poprad,sk", 1234567L, "45546454sdafa54asdf", 8019);
        //meteoStationDao.createMeteoDao(hibernate.getSessionFactory()).save(new MeteoStation("name", "password", "location", 1234567L, "apiKey", 1111));
    }
}
