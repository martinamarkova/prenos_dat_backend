package sk.fri.uniza.db;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import sk.fri.uniza.api.JSONWeatherData;
import sk.fri.uniza.api.Paged;

import java.util.*;

/**
 * objekt reprezentujúci databázu počasia
 */
public class JSONWeatherDataDao extends AbstractDAO<JSONWeatherData> implements BasicDao<JSONWeatherData, Long>{
    private static final List<JSONWeatherData> weatherDataDB = new ArrayList<>();
    private static JSONWeatherDataDao weatherDataDao;

    //static {
    //    JSONWeatherData weatherData = new JSONWeatherData(564L, "Presov", "SK", "clear sky", 292.19D, 290.93D, 294.15D, 0, 1017, 77,7.7D);
    //    weatherDataDB.add(weatherData);
    //}

    /**
     * konštruktor databázy
     * @param sessionFactory
     */
    public JSONWeatherDataDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * vráti statický objekt weatherDao
     * @return
     */
    public static JSONWeatherDataDao getJSONWeatherDataDao(){
        return weatherDataDao;
    }

    /**
     *
     * @return lokálna databáza
     */
    public static List<JSONWeatherData> getJSONWeatherDataDB() {
        return weatherDataDB;
    }

    /**
     * ak neexistu tak vytvori nový
     * @param sessionFactory
     * @return statický objekt JSONWeatherDataDao
     */
    public static JSONWeatherDataDao createJSONWeatherDataDao(SessionFactory sessionFactory) {
        if(weatherDataDao == null) {
            weatherDataDao = new JSONWeatherDataDao(sessionFactory);
        }
        return weatherDataDao;
    }

    // public Optional<MeteoStation> findById(Integer id) {
    //    MeteoStation meteoStation = get(id);
    //   return Optional.ofNullable(meteoStation);
    //}

    /**
     * najde počasie podla jedinešného id
     * @param id
     * @return
     */
    @Override
    public Optional<JSONWeatherData> findById(Long id) {
        JSONWeatherData weatherData = get(id);
        return Optional.ofNullable(weatherData);
    }

    /**
     *
     * @return celý zoznam počasia
     */
    @Override
    public List<JSONWeatherData> getAll() {
        return list(namedQuery("sk.fri.uniza.api.getAllJSONWeatherData"));
    }

    /**
     *
     * @param limit
     * @param page
     * @return stránkovaný celý zoznam počasia
     */
    @Override
    public Paged<List<JSONWeatherData>> getAll(int limit, int page) {
        String countQ = "Select count (f.id) from JSONWeatherData f";
        Query countQuery = currentSession().createQuery(countQ);
        Long countResults = (Long) countQuery.uniqueResult();

        int lastPageNumber = (int) (Math.ceil((float) countResults / limit));

        Query selectQuery = query("FROM JSONWeatherData ORDER BY idu DESC");
        selectQuery.setFirstResult((page - 1) * limit);
        selectQuery.setMaxResults(limit);
        List<JSONWeatherData> weatherDataPage = list(selectQuery);

        return new Paged<List<JSONWeatherData>>(page, limit, countResults, weatherDataPage);
    }

    /**
     *
     * @param limit
     * @param page
     * @param meteoStationId
     * @return stránkovaný celý zoznam počasia s id jednej stanice
     */
    public Paged<List<JSONWeatherData>> getbyMeteoStationID(int limit, int page, Long meteoStationId) {
        if (meteoStationId == null) {
            meteoStationId = 0L;
        }
        String countQ = "Select count (f.idu) from JSONWeatherData f WHERE id = " + meteoStationId.toString()+ " ORDER BY idu DESC";
        Query countQuery = currentSession().createQuery(countQ);
        Long countResults = (Long) countQuery.uniqueResult();

        int lastPageNumber = (int) (Math.ceil((float) countResults / limit));

        String queryString = "FROM JSONWeatherData WHERE id = " + meteoStationId.toString() + " ORDER BY idu DESC";
        Query selectQuery = query(queryString);
        selectQuery.setFirstResult((page - 1) * limit);
        selectQuery.setMaxResults(limit);
        List<JSONWeatherData> weatherDataPage = list(selectQuery);

        return new Paged<List<JSONWeatherData>>(page, limit, countResults, weatherDataPage);
    }


    /**
     * testovacie účely
     * @param meteoStationID
     * @return
     */
    public List<Map<String, Double>> getTemperatureInMeteoStation(Long meteoStationID) {
        String queryString = "FROM JSONWeatherData WHERE id = " + meteoStationID.toString()+ " ORDER BY idu DESC";

        Query selectQuery = this.currentSession().createQuery(queryString);

        List<JSONWeatherData> list = list(selectQuery);

        List<Map<String, Double>> result = new ArrayList<>();

        for(int i = 0; i< list.size(); i++){
            JSONWeatherData data = list.get(i);
            Map<String, Double> map = new HashMap<>();
            map.put(data.getAdded(), data.getTemp());
            result.add(map);
        }
        return  result;
    }

    /**
     * uloži obekt počasia do DB
     * @param weatherData
     * @return
     */
    @Override
    public Long save(JSONWeatherData weatherData) {
        return persist(weatherData).getId();
    }

    /**
     * update obekt počasia do DB
     * @param weatherData
     * @return
     */
    @Override
    public Long update(JSONWeatherData weatherData, String[] params) {
        persist(weatherData);
        return weatherData.getId();
    }

    /**
     * vymaže objekt počasia z db
     * @param weatherData
     */
    @Override
    public void delete (JSONWeatherData weatherData) {
        currentSession().delete(weatherData);
    }
}
