package sk.fri.uniza.db;

import com.google.common.collect.ImmutableSet;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import sk.fri.uniza.api.Location;
import sk.fri.uniza.api.MeteoStation;
import sk.fri.uniza.api.Paged;
import sk.fri.uniza.auth.APIKEYS;

import java.util.*;

/**
 * objekt pracujúci s databázou pre meteostanice
 */
public class MeteoStationDao extends AbstractDAO<MeteoStation> implements BasicDao<MeteoStation, Long>{
    /** lokálny objekt databázy */
    private static final List<MeteoStation> meteoStationDB = new ArrayList<>();
    private static MeteoStationDao meteoStationDao;

    // už sa nevyužíva tento statický blok
    static {
        MeteoStation meteoStation = new MeteoStation("Zilina", "hesloZilina", Location.ZILINA.getLocationToString(), Location.ZILINA.getId(), ImmutableSet.of(APIKEYS.API_KEY_ZILINA), Location.ZILINA.getAppConectPort());
        meteoStationDB.add(meteoStation);
    }

    /**
     * koštruktor
     * @param sessionFactory
     */
    public MeteoStationDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * statický geter statického objektu meteostationDao
     * @return
     */
    public static MeteoStationDao getMeteoStationDao(){
        return meteoStationDao;
    }

    /**
     * pred každou poziadavkov na databázu sa snaží vytiahnuť všetky meteostanice a ak niesu lokalne uložené tak ich uloži
     * @return
     */
    public List<MeteoStation> loadMeteoStastion() {

        Query selectQuery = this.currentSession().createQuery("FROM MeteoStation");

        List<MeteoStation> list = list(selectQuery);

        MeteoStation meteoStation = null;
        for (int i = 0; i < list.size();i++) {
            meteoStation = list.get(i);

            Optional<MeteoStation> meteoStationFM = findByName(meteoStation.getName());
            if (!meteoStationFM.isPresent()) {
                meteoStationDB.add(new MeteoStation(meteoStation));
            }
        }

        return  meteoStationDB;
    }

    /**
     * vráti lokálnu databázu meteostaníc
     * @return
     */
    public static List<MeteoStation> getMeteoStationDB() {
        return meteoStationDB;
    }

    /**
     * vráti ak nieje vytvorený vytvori nový
     * @param sessionFactory
     * @return objet meteoStationDao
     */
    public static MeteoStationDao createMeteoDao(SessionFactory sessionFactory) {
        if(meteoStationDao == null) {
            meteoStationDao = new MeteoStationDao(sessionFactory);
        }
        return meteoStationDao;
    }

    // public Optional<MeteoStation> findById(Integer id) {
    //    MeteoStation meteoStation = get(id);
    //   return Optional.ofNullable(meteoStation);
    //}

    /**
     * pokúsi sa najst meteostanicu podla ID
     * @param id
     * @return najdena meteostanica ak sa nájde
     */
    @Override
    public Optional<MeteoStation> findById(Long id) {
        MeteoStation meteoStation = get(id);
        return Optional.ofNullable(meteoStation);
    }

    /**
     * pokúsi sa najst meteostanicu podla mena
     * @param name
     * @return najdena meteostanica ak sa nájde
     */
    public Optional<MeteoStation> findByName (String name) {
        //MeteoStation meteoStation = get(name);
        MeteoStation meteoStation = null;
        for (int i = 0; i < meteoStationDB.size();i++) {
            if (meteoStationDB.get(i).getName().equals(name)) {
                meteoStation = meteoStationDB.get(i);
            }
        }
        return Optional.ofNullable(meteoStation);
    }

    /**
     * vráti zoznam všetkých meteostaníc
     * @return
     */
    @Override
    public List<MeteoStation> getAll() {
        return list(namedQuery("sk.fri.uniza.api.getAllMeteoStation"));
    }

    /**
     * vráti celý zoznam meteostaníc ale stránkovaný
     * @param limit
     * @param page
     * @return
     */
    @Override
    public Paged<List<MeteoStation>> getAll(int limit, int page) {
        String countQ = "Select count (f.id) from MeteoStation f";
        Query countQuery = currentSession().createQuery(countQ);
        Long countResults = (Long) countQuery.uniqueResult();

        int lastPageNumber = (int) (Math.ceil((float) countResults / limit));

        Query selectQuery = query("FROM MeteoStation ");
        selectQuery.setFirstResult((page - 1) * limit);
        selectQuery.setMaxResults(limit);
        List<MeteoStation> meteoStationPage = list(selectQuery);

        return new Paged<List<MeteoStation>>(page, limit, countResults, meteoStationPage);
    }

    /**
     * uloží meteostanicu do databázy
     * @param meteoStation
     * @return
     */
    @Override
    public Long save(MeteoStation meteoStation) {
        Long status = persist(meteoStation).getId();
        //meteoStationDB.add(meteoStation);
        return  status;
    }

    @Override
    public Long update(MeteoStation meteoStation, String[] params) {
        persist(meteoStation);
        return meteoStation.getId();
    }

    /**
     * vymaže meteostanicu z databázy
     * @param meteoStation
     */
    @Override
    public void delete (MeteoStation meteoStation) {
        currentSession().delete(meteoStation);
    }

}
